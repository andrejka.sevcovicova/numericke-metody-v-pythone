import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import PPoly
from scipy.interpolate import CubicSpline
from scipy.interpolate import interp1d

'''
Funkcie využívané v tejto kapitole
'''
def newton(x,y):
    #funkcia vypočíta koeficienty Newtonowho polynómu pre zadané x a y
    n = len(x)
    dif = np.zeros([n,n+1])
    dif[:,0] = x
    dif[:,1] = y

    for j in range(2,n+1):
        for i in range(0,n-j+1):
            dif[i,j] = (dif[i+1,j-1] - dif[i,j-1])/(dif[i+j-1,0] - dif[i,0])

    return(dif[0,1:n+1])

def newton_polyval(x,y,t):
    #funkcia vypočíta hodnotu Newtonowho polynómu v bode t (prípadne bodoch, ak t je vektor) pre zadané x a y
    a = newton(x,y)
    tval = a[0]*np.ones(np.size(t))
    prod = np.ones(np.size(t))

    for i in range(1,np.size(a)):
        prod = prod*(t-x[i-1])
        tval = tval + a[i]*prod
    return(tval)

def cebysevove_uzly(a,b,n):
    #vypočíta n Čebyševových uzlov pre interval [a,b]
    k = np.linspace(n-1,0,n)
    return((b-a)/2)*np.cos(np.pi*(2*k+1)/(2*n)) + (b+a)/2

def kubicky_splajn(x,y,deriv=None): #derivacia je volitelny argument
    n = len(x)
    a = y[0:n-1]
    h = x[1:n] - x[0:n-1]
    if deriv is None:
        ps = np.concatenate([np.array([0]), 3*((y[2:n]-y[1:n-1])/h[1:n-1]-(y[1:n-1]-y[0:n-2])/h[0:n-2]), np.array([0])])
        A = np.diag(np.concatenate([np.array([1]),2*(h[0:n-2]+h[1:n-1]),np.array([1])])) + np.diag(np.concatenate([h[0:n-2],np.array([0])]),-1) + np.diag(np.concatenate([np.array([0]),h[1:n-1]]),1)
    else:
        ps = np.concatenate([np.array([3*((y[1]-y[0])/h[0]-deriv[0])]), 3*((y[2:n]-y[1:n-1])/h[1:n-1]-(y[1:n-1]-y[0:n-2])/h[0:n-2]), np.array([3*(deriv[1]-(y[n-1]-y[n-2])/h[n-2])])])
        A = np.diag(np.concatenate([np.array([2*h[0]]),2*(h[0:n-2]+h[1:n-1]), np.array([2*h[n-2]])])) + np.diag(h[0:n-1],-1) + np.diag(h[0:n-1],1);

    c = np.linalg.solve(A,ps)
    d = (c[1:n]-c[0:n-1])/(3*h)
    b = (y[1:n]-y[0:n-1])/h -h*(2*c[0:n-1]+c[1:n])/3;
    koeficienty = [d,c[0:n-1],b,a]
    return PPoly(koeficienty,x)


'''
Riešené príklady
'''

#priklad 2.1.1.
x = np.array([-2, -1, 1, 3])
y = np.array([-3, 4, 3, 62])

V = np.vander(x)
a = np.linalg.solve(V,y)

t = np.linspace(-2,3,60)
plt.plot(t, np.polyval(a,t),x, y, 'ro')
plt.legend(['interpolant','dátové body'])
plt.grid()
plt.show()

#priklad 2.1.2.
x = np.array([-1, -1/2, 0, 1/2, 1])
y = np.exp(-x**2)

V = np.vander(x)
a = np.linalg.solve(V,y)

t = np.linspace(-1,1,1000)
plt.plot( t, np.polyval(a,t),t,np.exp(-t**2),x, y, 'ro')
plt.legend(['interpolant','presná funkcia','dátové body'])
plt.grid()
plt.xlabel('x')
plt.ylabel('f(x)')
plt.show()

abs_chyba = abs(np.exp(-t**2)-np.polyval(a,t));
rel_chyba = abs_chyba/np.exp(-t**2);
print('Absolutna chyba je:',max(abs_chyba))
print('Relativna chyba je:',max(rel_chyba))

plt.figure()
plt.plot(t, abs_chyba)
plt.grid()
plt.xlabel('x')
plt.ylabel('absolútna chyba')
plt.show()

#priklad 2.1.3.
n = 20
x = np.linspace(1,n,n)
y = np.zeros(n)

for i in range(0,n,2):
    y[i] = 1

V = np.vander(x)
a = np.linalg.solve(V,y)

t = np.linspace(1,n,1000)
plt.plot( t, np.polyval(a,t),x, y, 'ro' )
plt.grid()
plt.xlabel('x')
plt.ylabel('f(x)')
plt.show()

print('Hodnota pre p(20) je:',np.polyval(a,20)) 

#priklad 2.1.4.
x = np.array([1, 2, 4, 5])
y = np.array([1, 3, 5, 3])
print('Koeficienty pre Newtonow tvar polynomu su: ', newton(x,y))

#priklad 2.1.5.
n = 20
x = np.linspace(1,n,n)
y = np.zeros(n)

for i in range(0,n,2):
    y[i] = 1

t = np.array([20])
print('Hodnota pre p(20) je:',newton_polyval(x,y,t))

#priklad 2.1.6.
def f(x):
    return np.cos(x)

a = -3
b = 3
n = 4

x_r = np.linspace(a,b,n)
x_ch = cebysevove_uzly(a,b,n)
y_r = f(x_r)
y_ch = f(x_ch)
t = np.linspace(a,b,400)

p_r = np.polyfit(x_r,y_r,n-1)
p_ch = np.polyfit(x_ch,y_ch,n-1)

abs_odchylka_r = abs(f(t)-np.polyval(p_r,t))
abs_odchylka_ch = abs(f(t)-np.polyval(p_ch,t))

plt.plot(t,np.polyval(p_r,t), t,f(t),'g', x_r,y_r,'ro' )
plt.grid()
plt.legend(['interpolant','cos(x)','dátové body'])
plt.show()

plt.figure()
plt.plot(t, abs_odchylka_r)
plt.legend(['absolútna odchýlka'])
plt.grid()
plt.show()

print('Maximálna chyba je: ',max(abs_odchylka_r),' pri rovnomernom delení uzlov.')
print('Maximálna chyba je: ',max(abs_odchylka_ch),' pri Čebyševových uzloch.')

#priklad 2.1.7.
x = np.array([-2, -1, 1, 3])
y = np.array([-3, 4, 3, 62])

t = np.linspace(-2,3,60)
plt.plot(t, (4*t**3 +3*t**2 -5*t +4)/2, x, y, 'ro' )
plt.legend(['interpolant','dátové body'])
plt.grid()
plt.show()

plt.figure()
plt.plot(t, -(t**3 -3*t**2 -t +3)/15,
         t, (t**3 -2*t**2-5*t+6)/8,
         t, -(t**3-7*t-6)/12,
         t, (t**3+2*t**2-t-2)/40 )
plt.legend(['L0','L1','L2','L3'])
plt.grid()
plt.show()

#priklad 2.1.8.
def Q(t,x):
    prod = np.ones(np.size(t))
    for i in range(0,np.size(x)):
        prod = np.multiply(prod,(t-x[i]))
    return abs(prod)

a = -1
b = 1
n = 10
x_r = np.linspace(a,b,n)
x_ch = cebysevove_uzly(a,b,n)

t = np.linspace(a,b,1000)
plt.plot(t,Q(t,x_r), t,Q(t,x_ch))
plt.legend(['funkcia Q(x) pre rovnomerne delené uzly','funkcia Q(x) pre Čebyševove uzly'])
plt.grid()
plt.show()

n = 8
x_r = np.linspace(a,b,n)

t = np.linspace(a,b,1000)
plt.figure()
plt.plot(t,Q(t,x_r), x_r, np.zeros(n),'ro')
plt.legend(['funkcia Q(x)','dátové body'])
plt.grid()
plt.show()

#priklad 2.1.9.
def T0(x):
    return np.ones(np.size(x))
def T1(x):
    return x
def T2(x):
    return 2*x**2 - 1
def T3(x):
    return 4*x**3 - 3*x
def T4(x):
    return 8*x**4 - 8*x**2 + 1
def T5(x):
    return 16*x**5 - 20*x**3 + 5*x

t = np.linspace(-1,1,200)
plt.plot(t,T0(t),
         t,T1(t),
         t,T2(t),
         t,T3(t),
         t,T4(t),
         t,T5(t))
plt.grid()
plt.legend(['T0','T1','T2','T3','T4','T5'])
plt.show()

#priklad 2.1.10.
k = np.linspace(4,0,5)
x = np.cos(np.pi*(2*k+1)/10);
y = (3/2)*x + 7/2
print('Čebyševove uzly pre interval [2,5] s 5 uzlami sú: ',y)

#priklad 2.2.1
a = -1
b = 1
n = 5
x = np.linspace(a,b,n)
y = np.sign(x)

V = np.vander(x)
IP = np.linalg.solve(V,y)
t = np.linspace(-1,1,200)

plt.plot(np.array([-1,0]),np.array([-1,-1]),color = '#1f77b4', label= 'sgn(x)')
plt.plot(np.array([0,1]),np.array([1,1]),color = '#1f77b4')
plt.plot(x,y,'-',color = '#ff7f0e',label = 'lineárny splajn')
plt.plot(t,np.polyval(IP,t),color = '#2ca02c',label = 'IP stupňa 4')
plt.plot(x,y,'ro',color = '#d62728',label = 'dátové body')
plt.legend()
plt.grid()
plt.show()

#priklad 2.2.2.
a = -3
b = 3
n = 4
x = np.linspace(a,b,n)
y = np.cos(x)
lins = interp1d(x, y, kind='linear')

t = np.linspace(a,b,1000)
plt.plot(t,np.cos(t),t,lins(t),x,y,'ro')
plt.legend(['presná funkcia','lineárny splajn','dátové body'])
plt.grid()
plt.show()

abs_odchylka = abs(np.cos(t) - lins(t))
print('Najväčšia chyba je',max(abs_odchylka))

#priklad 2.2.3.
x = np.array([1,2,4])
y = np.array([1,3,5])
#cs = CubicSpline(x,y,axis=0, bc_type='natural')
cs = kubicky_splajn(x,y)

t = np.linspace(1,4,1000)
plt.plot(t,cs(t),x,y,'ro') 
plt.grid()
plt.legend(['prirodzený kubický splajn','dátové body'])
plt.show()

#priklad 2.2.4.
x = np.arange(5)
y = np.array([0,1,0,2,4])
derivacia = np.array([0,0])
#cs = CubicSpline(x,y,axis=0, bc_type='clamped')
cs_clamped = kubicky_splajn(x,y,derivacia)
cs_natural = kubicky_splajn(x,y)

t = np.linspace(0,4,1000)
plt.plot(t,cs_clamped(t),t,cs_natural(t),x,y,'ro') 
plt.grid()
plt.legend(['clamped kubický splajn','prirodzený kubický splajn','dátové body'])
plt.show()
