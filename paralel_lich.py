# priklad 5.3.6.
# seriovy pristup
import numpy as np
import time

def f(x):
    return 1/x
a = 1
b = 5    
n = 1000000

def lich(i):
    h = (b-a)/n
    return h*(f(a+h*i) + f(a+h*(i+1)))/2

t0 = time.time()
part = []
for i in range(n): 
      part.append(lich(i))
vysledok = sum(part)
t1 = time.time()
print(f'Výsledok: {vysledok}')
print(f'Čas trvania výpočtu: {t1 - t0} s')   

# paralelny pristup
import multiprocessing as mp

t0 = time.time()
vysledok = []
n_cpu = mp.cpu_count()

pool = mp.Pool(processes=n_cpu)
vysledok = sum(pool.map(lich, range(n)))

t1 = time.time()
print(f'Výsledok: {vysledok}')
print(f'Čas trvania výpočtu: {t1 - t0} s')  


# priklad 5.3.7.
import matplotlib.pyplot as plt
from functools import partial

plt.rc('font', size=20)         # controls default text sizes
plt.rc('axes', titlesize=20)    # fontsize of the axes title
plt.rc('axes', labelsize=40)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=20)   # fontsize of the tick labels
plt.rc('ytick', labelsize=20)   # fontsize of the tick labels
plt.rc('legend', fontsize=30)   # legend fontsize
plt.rc('figure', titlesize=12)  # fontsize of the figure title

def lich_n(n,i):                                     
    h = (b-a)/n
    return h*(f(a+h*i) + f(a+h*(i+1)))/2

def serial(n):
    t0 = time.time()
    part = []
    for i in range(n): 
        part.append(lich_n(n,i))
    vysledok = sum(part)
    t1 = time.time()
    exec_time = t1 - t0
    return(vysledok,exec_time)

def parallel(n):
    t0 = time.time()
    n_cpu = mp.cpu_count()

    pool = mp.Pool(processes=n_cpu)
    func = partial(lich_n, n)                        
    vysledok = sum(pool.map(func, range(n)))
    t1 = time.time()
    exec_time = t1 - t0
    return(vysledok,exec_time)
    
n_run = np.logspace(1, 7, num = 7)

t_serial = [serial(int(n))[1] for n in n_run]
t_parallel = [parallel(int(n))[1] for n in n_run]

plt.plot(n_run, t_serial, '-o', label = 'sériový prístup',linewidth=4)
plt.plot(n_run, t_parallel, '-o', label = 'paralelný prístup',linewidth=4)
plt.loglog()
plt.legend()
plt.grid()
plt.ylabel('Čas trvania výpočtu (s)')
plt.xlabel('Počet podintervalov')
plt.show()

print('Sériový prístup pre n=1000',serial(1000))
print('Sériový prístup pre n=100000',serial(100000))
print('Sériový prístup pre n=10000000',serial(10000000))

print('Paralelný prístup pre n=1000',parallel(1000))
print('Paralelný prístup pre n=100000',parallel(100000))
print('Paralelný prístup pre n=10000000',parallel(10000000))

