import numpy as np
import matplotlib.pyplot as plt

'''
Funkcie využívané v tejto kapitole
'''

def bisekcia(a,b,N,f):
    iteracie = np.zeros(N)
    for i in range(0,N): 
        if f(a)*f(b) < 0:
            c = (a + b)/2
            iteracie[i] = c
            if f(a)*f(c) < 0:
                b = c
            else:
                a = c
        elif f(a) == 0:
                c = a
                break
        else:
                c = b
                break   
    return(c,iteracie)                           

def prosta_iteracia(x0,N,g):
    x = np.zeros(N+1)
    x[0] = x0
    for i in range(0,N):                            
        x[i+1] = g(x[i])
            
    return(x[N],x)

def newton(x0,N,f,df):
    x = np.zeros(N+1)
    x[0] = x0
    for i in range(0,N):                            
        x[i+1] = x[i] - f(x[i])/df(x[i])
            
    return(x[N],x)                       

def newton_mod(x0,x1,N,f):                       
    x = np.zeros(N+2)                               
    x[0] = x0
    x[1] = x1
    df = np.zeros(N)
    for i in range(0,N):                            
        if f(x[i+1]) - f(x[i]) == 0:
            break
        df[i] = (f(x[i+1]) - f(x[i]))/(x[i+1] - x[i])
        x[i+2] = x[i+1] - f(x[i+1])/df[i]
        
    return(x[N+1],x)                            

def newton_mod2(x0,N,f,h):
    x = np.zeros(N+1)
    df = np.zeros(N)
    x[0] = x0
    for i in range(0,N):                            
        df[i] = (f(x[i] + h) - f(x[i]))/h
        x[i+1] = x[i] - f(x[i])/df[i]
        
    return(x[N],x)  

def F(x,y):
    return np.array([f(x,y),g(x,y)])

def J(x,y):
    return np.array([[fx(x,y),fy(x,y)],[gx(x,y),gy(x,y)]])

'''
Riešené príklady
'''

#priklad 4.3.1.
t = np.linspace(0,5,500)
plt.plot(t,np.exp(t),t,t+20)
plt.grid()
plt.legend(['y = exp(x)','y = x + 20'])
plt.xlabel('x')
plt.ylabel('y')
plt.show()             

def g(x):
    return np.exp(x) - x - 20
    
a = 0
b = 5
N = 50
[x,iteracie] = bisekcia(a,b,N,g)

#priklad 4.3.2.
N = 20
x = np.zeros(N+1)
x[0] = 1

for i in range(0,N):                                
    x[i+1] = np.cos(x[i])


t = np.linspace(0,np.pi/2,200)
plt.plot(t,np.cos(t),t,t)
plt.grid()
plt.xlabel('x')
plt.ylabel('y')
plt.legend(['y=cos(x)','y=x'])

for i in range(0,N):
    plt.plot([x[i],x[i],x[i+1]],[x[i],x[i+1],x[i+1]],color='#d62728')
    
plt.show()

#priklad 4.3.3.
def g1(x):
    return 2 - x*np.exp(x) + x

def g2(x):
    return 2/np.exp(x)

def g3(x):
    return x + (2 - x*np.exp(x))/4

[x_g1, iter_g1] = prosta_iteracia(1,50,g1)                            
[x_g2, iter_g2] = prosta_iteracia(1,50,g2)
[x_g3, iter_g3] = prosta_iteracia(1,50,g3)

t = np.linspace(0,1,500)
plt.plot(t,t, t,g1(t), t,g2(t), t,g3(t))
plt.grid()
plt.xlabel('x')
plt.ylabel('y')
plt.legend(['y=x','y=2-x*exp(x)+x','y=2/exp(x)','y=x+(2 - x*exp(x))/4'])
plt.show()

#priklad 4.3.4.
def f(x):
    return x - np.sin(x) - 1/4
def df(x):
    return 1 - np.cos(x)

N = 6
x0 = 1
[x,iteracie] = newton(x0,N,f,df)

x_presne = 1.17122965250167
abs_chyba = np.zeros(N+1)
for i in range(0,N+1):
    abs_chyba[i] = abs(iteracie[i] - x_presne)

plt.semilogy(np.linspace(0,N,N+1),abs_chyba,'.-')
plt.grid()
plt.xlabel('n')
plt.ylabel(r'$|x_{n} - x_{presne}|$')
plt.show()

def g(x):
    return np.sin(x) + 1/4

a = 0
b = 2
x0 = 1
x1 = 1.5
N = 15
h = 0.5

[x_b, iter_b] = bisekcia(a,b,N,f)
[x_p, iter_p] = prosta_iteracia(x0,N,g)
[x_n, iter_n] = newton_mod(x0,x1,N,f)                              
[x_n2, iter_n2] = newton_mod2(x0,N,f,h)       

#priklad 4.3.5.
def f(x,y):
    return x**2 + y**2 - x
def g(x,y):
    return x**2 - y**2 - y

def fx(x,y):
    return 2*x -1
def fy(x,y):
    return 2*y
def gx(x,y):
    return 2*x
def gy(x,y):
    return -2*y -1

N = 6
x0 = np.array([1,1])
x = np.zeros([2,N+1])
x[:,0] = x0

for i in range(0,N):
    x[:,i+1] = np.linalg.solve(J(x[0,i],x[1,i]),(J(x[0,i],x[1,i])@x[:,i] - F(x[0,i],x[1,i])))

x_presne = np.array([0.77184450634604, 0.41964337760708])
abs(x[:,N] - x_presne) 

norma = np.zeros(N+1);
for i in range(0,N+1):
    chyba = x[:,i] - x_presne
    norma[i] = np.linalg.norm(chyba,2)

plt.semilogy(np.linspace(0,N,N+1),norma,'.-')
plt.grid()
plt.xlabel('n')
plt.ylabel(r'$||x_{n}-x_{presne}||_{2}$')
plt.show()

#priklad 4.3.6.
def f(x,y):
    return x*y - (x**2)/y + 1
def g(x,y):
    return y*np.exp(x) - 2

def fx(x,y):
    return y - 2*x/y
def fy(x,y):
    return x + (x/y)**2
def gx(x,y):
    return y*np.exp(x)
def gy(x,y):
    return np.exp(x)

N = 6
#x0 = np.array([1,1])
x0 = np.array([-1,2])
x = np.zeros([2,N+1])
x[:,0] = x0

for i in range(0,N):
    x[:,i+1] = np.linalg.solve(J(x[0,i],x[1,i]),(J(x[0,i],x[1,i])@x[:,i] - F(x[0,i],x[1,i])))

#x_presne = np.array([1.08325678173557, 0.67698267233515])
x_presne = np.array([-0.34088062340218, 2.81237073043469]) 

abs(x[:,N] - x_presne) 

norma = np.zeros(N+1);
for i in range(0,N+1):
    chyba = x[:,i] - x_presne
    norma[i] = np.linalg.norm(chyba,2)

plt.semilogy(np.linspace(0,N,N+1),norma,'.-')
plt.grid()
plt.xlabel('n')
plt.ylabel(r'$||x_{n}-x_{presne}||_{2}$')
plt.show()

                    
