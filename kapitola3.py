import numpy as np
import matplotlib.pyplot as plt

'''
Funkcie využívané v tejto kapitole
'''
def kubicky_splajn_A_b(x,y,deriv=None): #derivacia je optional argument
    n = len(x)
    h = x[1:n] - x[0:n-1]
    if deriv is None:
        ps = np.concatenate([np.array([0]), 3*((y[2:n]-y[1:n-1])/h[1:n-1]-(y[1:n-1]-y[0:n-2])/h[0:n-2]), np.array([0])])
        A = np.diag(np.concatenate([np.array([1]),2*(h[0:n-2]+h[1:n-1]),np.array([1])])) + np.diag(np.concatenate([h[0:n-2],np.array([0])]),-1) + np.diag(np.concatenate([np.array([0]),h[1:n-1]]),1)
    else:
        ps = np.concatenate([np.array([3*((y[1]-y[0])/h[0]-deriv[0])]), 3*((y[2:n]-y[1:n-1])/h[1:n-1]-(y[1:n-1]-y[0:n-2])/h[0:n-2]), np.array([3*(deriv[1]-(y[n-1]-y[n-2])/h[n-2])])])
        A = np.diag(np.concatenate([np.array([2*h[0]]),2*(h[0:n-2]+h[1:n-1]), np.array([2*h[n-2]])])) + np.diag(h[0:n-1],-1) + np.diag(h[0:n-1],1);

    return [A,ps]

def cgm(A,b,x0,N):
    x = x0
    r = b - A@x
    s = r
    
    for i in range(0,N+1):
        alfa = (r@r)/(s@A@s)
        x = x + alfa*s
        r_old = r@r
        r = r - alfa*A@s
        beta = (r@r)/r_old
        s = r + beta*s
        
    return x

def jacobi(A,b,x0,N):
    D = np.diag(np.diag(A))
    L = np.tril(A,-1)
    U = np.triu(A,1)
    T = -np.linalg.inv(D)@(L+U)
    c = np.linalg.inv(D)@b

    Y = np.zeros((N+1,np.size(x0)))
    Y[0] = x0
    for i in range(0,N):
        Y[i+1] = T@Y[i] + c

    return(Y[N],Y)

def gs(A,b,x0,N):
    iteracie = np.zeros([N+1,np.size(x0)])
    x = x0
    iteracie[0] = x0
    for k in range (0,N):
        for i in range (0,np.size(x0)):
            x[i] = (b[i] - A[i,:]@x + A[i,i]*x[i])/A[i,i]
        iteracie[k+1] = x

    return(x, iteracie)

def R(T):
    return np.log(1/max(abs(np.linalg.eigvals(T))))

def spekt_polomer(T):
    return max(abs(np.linalg.eigvals(T)))

'''
Riešené príklady
'''

#priklad 3.6.1.
n = 200
A1 = np.array([[4/3, 2, -2],[1, 1, 1],[2, 2, 1]])
b = np.array([1, 0, 0])

T_n1 = np.identity(3)-A1
c = b
X = np.zeros((n+1,3))

for i in range(0,n):
    X[i+1] = T_n1@X[i] + c

#chyba pre N-tu iteraciu
x_presne = np.linalg.solve(A1,b)
print('Chyba pre ',n,'-tú iteráciu je: ',np.linalg.norm(X[n]-x_presne,2))

# vyvoj chyby
norm_X = np.zeros((n+1,1))

for i in range(0,n+1):
    chyba = X[i]-x_presne
    norm_X[i] = np.linalg.norm(chyba, 2)

plt.semilogy(np.linspace(0,n,n+1),norm_X,'.-')
plt.grid()
plt.xlabel('k')
plt.ylabel(r'$||x_{k} - x_{presne}||_{2}$')
plt.show()

D = np.diag(np.diag(A1))
L = np.tril(A1,-1)
U = np.triu(A1,1)
T_j1 = -np.linalg.inv(D)@(L+U)

#priklad 3.6.2.
A2 = np.array([[1, 2, -1],[0, 2, -1],[-1/2, 1, -1]])
b = np.array([1, 1, 1])
x0 = np.array([0, 0, 0])

N = 200
D = np.diag(np.diag(A2))
L = np.tril(A2,-1)
U = np.triu(A2,1)
T_gs = -np.linalg.inv(L+D)@U
T_j2 = -np.linalg.inv(D)@(L+U)
c_gs = np.linalg.inv(L+D)@b
c_j = np.linalg.inv(D)@b
X_gs = np.zeros((N+1,3))
X_j = np.zeros((N+1,3))

for i in range (0,N):
    X_gs[i+1] = T_gs@X_gs[i] + c_gs
    X_j[i+1] = T_j2@X_j[i] + c_j

x_presne = np.linalg.solve(A2,b)
print('Chyba pre Jacobiho metódu je ',abs(X_j[N]-x_presne))
print('Chyba pre G-S metódu je ',abs(X_gs[N]-x_presne))

norm_X_gs = np.zeros((N+1,1))
norm_X_j = np.zeros((N+1,1))

for i in range (0,N+1):
    chyba_gs = X_gs[i] - x_presne
    chyba_j = X_j[i] - x_presne
    norm_X_gs[i] = np.linalg.norm(chyba_gs, 2)
    norm_X_j[i] = np.linalg.norm(chyba_j, 2)

plt.semilogy(np.linspace(0,N,N+1),norm_X_gs,'.-',np.linspace(0,N,N+1),norm_X_j,'.-')
plt.grid()
plt.legend(['Gaussova-Seidelova metóda','Jacobiho metóda'])
plt.xlabel('k')
plt.ylabel(r'$||x_{k} - x_{presne}||_{2}$')
plt.show()

T_n2 = np.identity(3)-A2

#priklad 3.6.3.
print('Spektrálny polomer matice T z príkladu 3.6.1 pre naivnú metódu sú',spekt_polomer(T_n1)) 
print('Spektrálny polomer matice T z príkladu 3.6.1 pre Jacobiho metódu sú',spekt_polomer(T_j1)) 
print('Spektrálny polomer matice T z príkladu 3.6.2 pre naivnú metódu sú',spekt_polomer(T_n2)) 
print('Spektrálny polomer matice T z príkladu 3.6.2 pre Jacobiho metódu sú',spekt_polomer(T_j2)) 

#priklad 3.6.4.
# naivna metoda
A = np.array([[2, 2, 2],[3, 2, 1],[-2, 2, 2]])
T_n4 = np.identity(3)-A
lambda_n4 = np.linalg.eigvals(T_n4)
print('Absolútne hodnoty vlastných hodnôt matice pre naivnú metódu sú',abs(lambda_n4))

# Jacobiho metoda
D = np.diag(np.diag(A))
L = np.tril(A,-1)
U = np.triu(A,1)
T_j4 = -np.linalg.inv(D)@(L+U)
lambda_j4 = np.linalg.eigvals(T_j4)
print('Absolútne hodnoty vlastných hodnôt matice pre Jacobiho metódu sú',abs(lambda_j4)) 

#priklad 3.6.5.
print('Norma matice, z príkladu 3.6.1, pre naivnú metódu je',np.linalg.norm(T_n1,2))
print('Norma matice, z príkladu 3.6.1, pre Jacobiho metódu je',np.linalg.norm(T_j1,2)) 
print('Norma matice, z príkladu 3.6.2, pre naivnú metódu je',np.linalg.norm(T_n2,2)) 
print('Norma matice, z príkladu 3.6.2, pre Jacobiho metódu je',np.linalg.norm(T_j2,2))
print('Norma matice, z príkladu 3.6.4, pre naivnú metódu je',np.linalg.norm(T_n4,2)) 
print('Norma matice, z príkladu 3.6.4, pre Jacobiho metódu je',np.linalg.norm(T_j4,2)) 

#priklad 3.6.6.
x = np.linspace(0,4,5)
y = np.array([0,1,0,2,4])
derivacia = np.zeros(2)
[A,b] = kubicky_splajn_A_b(x,y,derivacia)
x0 = np.zeros(5)

x_cgm = cgm(A,b,x0,5)
x_presne = np.linalg.solve(A,b)

N = 100
[x_jm,iter_jm] = jacobi(A,b,x0,N)
[x_gsm,iter_gsm] = gs(A,b,x0,N)

normaX_jm = np.zeros(N)
normaX_gsm = np.zeros(N)
normaX_cgm = np.zeros(N)

for i in range (0,N):
    chyba_jm = iter_jm[i] - x_presne
    normaX_jm[i] = np.linalg.norm(chyba_jm, ord=2)
    chyba_gsm = iter_gsm[i] - x_presne
    normaX_gsm[i] = np.linalg.norm(chyba_gsm, ord=2)
    chyba_cgm = cgm(A,b,np.zeros(5),i) - x_presne
    normaX_cgm[i] = np.linalg.norm(chyba_cgm, ord=2)

plt.semilogy(np.linspace(0,N,N),normaX_jm,np.linspace(0,N,N),normaX_gsm,np.linspace(0,N,N),normaX_cgm)
plt.legend(['Jacobiho metóda','Gaussova-Seidelova metóda','Metóda konjugovaných gradientov'])
plt.grid()
plt.xlabel('k')
plt.ylabel(r'$||x_{k} - x_{presne}||_{2}$')
plt.show()

#priklad 3.6.7.
A1 = np.array([[4/3, 2, -2],[1, 1, 1],[2, 2, 1]])
A2 = np.array([[1, 2, -1],[0, 2, -1],[-1/2, 1, -1]])
A3 = np.array([[2, 2, 2],[3, 2, 1],[-2, 2, 2]])
A4 = np.array([[3, 0, -1],[-1, 2, 1],[1, -1, -1]])

A = A1
D = np.diag(np.diag(A))
L = np.tril(A,-1)
U = np.triu(A,1)

T_gs = -np.linalg.inv(L+D)@U
T_j = -np.linalg.inv(D)@(L+U)

print('Spektrálny polomer matice T pre Jacobiho metódu je',spekt_polomer(T_j))
print('Spektrálny polomer matice T pre G-S metódu je',spekt_polomer(T_gs))

print('Rýchlosť konvergencie iteračného procesu pre Jacobiho metódu je',R(T_j))
print('Rýchlosť konvergencie iteračného procesu pre G-S metódu je',R(T_gs))

#priklad 3.6.8.
N = 100
R_j = np.zeros(N-3)
R_gs = np.zeros(N-3)

for i in range(4,N+1):
    x = np.linspace(1,i,i)
    y = 10*np.random.random(i)
    [A,b] = kubicky_splajn_A_b(x,y)
    D = np.diag(np.diag(A))
    L = np.tril(A,-1)
    U = np.triu(A,1)
    T_j = -np.linalg.inv(D)@(L+U)
    R_j[i-4] = R(T_j)
    T_gs = -np.linalg.inv(L+D)@U
    R_gs[i-4] = R(T_gs)

plt.plot(np.linspace(4,N+1,N-3),R_gs,np.linspace(4,N+1,N-3),R_j)
plt.grid()
plt.legend(['Gaussova-Seidelova metóda','Jacobiho metóda'])
plt.xlabel('n')
plt.ylabel('R')
plt.show()

#priklad 3.6.9.
A = np.array([[6, 5, 2],[5, 9, -1],[2, -1, 3]])
b = np.array([-1, -3, 0])
x0 = np.zeros(3)

x = x0
r = b - A@x
s = r

n = 3
x_iter = np.zeros([np.size(x),n+1])

for i in range(0,n):
    alfa = (r@r)/(s@A@s)
    x_iter[:,i+1] = x_iter[:,i] + alfa*s
    r_old = r@r
    r = r - alfa*A@s
    beta = (r@r)/r_old
    s = r + beta*s

x_presne = np.linalg.solve(A, b)
print(abs(np.linalg.norm(x_iter[:,i+1]-x_presne, ord = 2)))

#priklad 3.6.10.
import scipy.sparse as sparse
n = 100
B = sparse.random(n, n, density=0.1)
A = B@B.T + sparse.identity(n)
A = A.toarray()
b = np.random.uniform(0,1,n)

x_presne = np.linalg.solve(A, b)

x_cgm = cgm(A,b,np.zeros(n),n)
[x_gs,iter_gs] = gs(A,b,np.zeros(n),n)
[x_j,iter_j] = jacobi(A,b,np.zeros(n),n)

print('Chyba aproximácie pre metódu konjugovaných gradientov je',np.linalg.norm(x_presne-x_cgm,2))
print('Chyba aproximácie pre G-S metódu je',np.linalg.norm(x_presne-x_gs,2))
print('Chyba aproximácie pre Jacobiho metódu je',np.linalg.norm(x_presne-x_j,2))

