import numpy as np 

## Teória
'''
Nájdite medzery okolo čísel 0.001, 1, 1000, 10000000 
a medzery okolo čísla 1 pre formáty float16, float32, float64 a float128.
'''
for i in [0.001, 1, 1000, 10000000]:
    print(f'Medzera pre {i:.4e} :\t {np.spacing(i):.4e}')

for i in [np.float16(1), np.float32(1), np.float64(1), np.float128(1)]:
    print(f'Medzera pre {i} je {np.spacing(i):.4e} {type(i)}')

'''
Nájdite počet platných cifier, ktoré Python rozoznáva.
'''
a = 1
for i in range(1,2000):
	a = a + 0.1**i
	if a == a + 0.1**i:
		print('počet platných cifier je: ',i)
		break

'''
Nájdite minimálny a maximálny exponent dátového typu float64.
'''
a = np.float64(1)
while 2**a != np.inf:
    a = a + 1
print('Maximálny exponent je: ', a-1)

b = np.float64(10)
while 2**b !=0:
    b = b - 1
print('Minimálny exponent je: ', b+1)

## Riesene priklady
#priklad 1.2.2.
def zao(a,x):
    if a == 0:
        return 0
    else:
        n = 1 + np.floor(np.log10(abs(a)))
        return round(a*(10**(x-n))) / 10**(x-n)

print('zao(8.53,1) = ',zao(8.53,1))
print('zao(-12800,2) = ',zao(-12800,2))
print('zao(0.03445,2) = ',zao(0.03445,2))
print('zao(0.1,2) = ',zao(0.1,2))

#priklad 1.3.3.
def zaoplus(a,b,x):
    a1 = zao(a,x)
    b1 = zao(b,x)
    return zao(a1+b1,x)

def zaominus(a,b,x):
    a1 = zao(a,x)
    b1 = zao(b,x)
    return zao(a1-b1,x)

def zaokrat(a,b,x):
    a1 = zao(a,x)
    b1 = zao(b,x)
    return zao(a1*b1,x)

def zaodelene(a,b,x):
    a1 = zao(a,x)
    b1 = zao(b,x)
    return zao(a1/b1,x)

print('zaoplus(8.53,120,2) = ',zaoplus(8.53,120,2))
print('zaoplus(0.15,0.15,1) = ',zaoplus(0.15,0.15,1))
print('zaokrat(12849,0.0013,3) = ',zaokrat(12849,0.0013,3))

#priklad 1.3.4.
z = 0
for k in range(1,10001):
    z = z + 1/(k**2)

z5 = 0
for k in range(1,10001):
    z5 = zaoplus(z5,zaodelene(1,zaokrat(k,k,5),5),5)

AbsChyba = abs(z5-z)
RelChyba = AbsChyba/abs(z)
print('Absolútna chyba: ',AbsChyba)
print('Relatívna chyba: ',RelChyba)

#priklad 1.3.5.
x = 1000000
for _ in range (0,1000):
    x = zaoplus(x,1,5)
print('Tisíckrát postupne prípočítaná 1ka k číslu 1000000 s presnosťou na 5 platných cifier je ',x)

#priklad 1.3.6.
print()
def vyraz(x):
    return zaominus(np.sqrt(zaoplus(zaokrat(x,x,5),1,5)),1,5)

print('Príklad 1.3.6. pre 0,1 =', vyraz(0.1),
      'Absolútna chyba =', zao(abs(vyraz(0.1) - np.sqrt(0.1**2 +1) +1),5),
      'Relatívna chyba =', zao(abs(vyraz(0.1) - np.sqrt(0.1**2 +1) +1)/abs(vyraz(0.1)),5))
print('Príklad 1.3.6. pre 0,07 =', vyraz(0.07),
      'Absolútna chyba =', zao(abs(vyraz(0.07) - np.sqrt(0.07**2 +1) +1),5),
      'Relatívna chyba =', zao(abs(vyraz(0.07) - np.sqrt(0.07**2 +1) +1)/abs(vyraz(0.07)),5))
print('Príklad 1.3.6. pre 0,03 =', vyraz(0.03),
      'Absolútna chyba =', zao(abs(vyraz(0.03) - np.sqrt(0.03**2 +1) +1),5),
      'Relatívna chyba =', zao(abs(vyraz(0.03) - np.sqrt(0.03**2 +1) +1)/abs(vyraz(0.03)),5))

def vyraz_upr(x):
    return zaodelene(zaokrat(x,x,5),zaoplus(np.sqrt(zaoplus(zaokrat(x,x,5),1,5)),1,5),5)

print('Upravený príklad 1.3.6. pre 0,1 =', vyraz_upr(0.1),
      'Absolútna chyba =', zao(abs(vyraz_upr(0.1) - np.sqrt(0.1**2 +1) +1),5),
      'Relatívna chyba =', zao(abs(vyraz_upr(0.1) - np.sqrt(0.1**2 +1) +1)/abs(vyraz_upr(0.1)),5))
print('Upravený príklad 1.3.6. pre 0,07 =', vyraz_upr(0.07),
      'Absolútna chyba =', zao(abs(vyraz_upr(0.07) - np.sqrt(0.07**2 +1) +1),5),
      'Relatívna chyba =', zao(abs(vyraz_upr(0.07) - np.sqrt(0.07**2 +1) +1)/abs(vyraz_upr(0.07)),5))
print('Upravený príklad 1.3.6. pre 0,03 =', vyraz_upr(0.03),
      'Absolútna chyba =', zao(abs(vyraz_upr(0.03) - np.sqrt(0.03**2 +1) +1),5),
      'Relatívna chyba =', zao(abs(vyraz_upr(0.03) - np.sqrt(0.03**2 +1) +1)/abs(vyraz_upr(0.03)),5))

