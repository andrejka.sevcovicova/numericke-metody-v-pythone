import numpy as np

'''
Funkcie využívané v tejto kapitole
'''
def lich(f,a,b,n):
    h = (b-a)/n
    I_lich = 0
    
    for i in range (0,n):
        I_lich = I_lich + h*(f(a+h*i) + f(a+h*(i+1)))/2
        
    return I_lich

def obdl(f,a,b,n):
    h = (b-a)/n
    I_obdl = 0
    
    for i in range (0,n):
        I_obdl = I_obdl + h*f((a+h*i + a+h*(i+1))/2)
        
    return I_obdl

def simp(f,a,b,n):
    h = (b-a)/n
    I_simp = 0
    
    for i in range (0,n):
        I_simp = I_simp + h*(f(a+h*i) + 4*f((a+h*i + a+h*(i+1))/2) + f(a+h*(i+1)))/6
        
    return I_simp

def f(x):
    return 1/x

'''
Riešené príklady
'''

#priklad 5.3.1.
a = 1
b = 5
I_lich = (1/2)*(b-a)*(f(a)+f(b))
I_simp = (1/6)*(b-a)*(f(a)+4*f((a+b)/2)+f(b))

chyba_lich = abs(np.log(5)-I_lich)
chyba_simp = abs(np.log(5)-I_simp)
rel_chyba_lich = abs(np.log(5)-I_lich)/abs(np.log(5))
rel_chyba_simp = abs(np.log(5)-I_simp)/abs(np.log(5))

#priklad 5.3.2.
chyba_lich = abs(np.log(5)-lich(f,1,5,4))
chyba_simp = abs(np.log(5)-simp(f,1,5,2))
chyba_obdl = abs(np.log(5)-obdl(f,1,5,2))
rel_chyba_lich = abs(np.log(5)-lich(f,1,5,4))/abs(np.log(5))
rel_chyba_simp = abs(np.log(5)-simp(f,1,5,2))/abs(np.log(5))
rel_chyba_obdl = abs(np.log(5)-obdl(f,1,5,2))/abs(np.log(5))

#priklad 5.3.3.
print('Hodnota získaná pomocou lichobežníkovej metódy pre n=327',lich(f,1,5,327))
#priklad 5.3.4.
print('Hodnota získaná pomocou Simpsonovej metódy pre n=18',simp(f,1,5,18))
#priklad 5.3.5.
print('Hodnota získaná pomocou obdĺžnikovej metódy pre n=231',obdl(f,1,5,231))
print('Presná hodnota ln(5) je',np.log(5))
